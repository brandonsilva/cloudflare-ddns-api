from datetime import datetime
import time
import requests

headers = {
    'X-Auth-Email': 'brandon@bsilva.dev',
    'X-Auth-Key': '23c7d0cc0cac8fcf1573266f3f9c5864eee7b',
    'Content-Type': 'application/json',
}


def dynamic_dns(domain, zoneid, subdomainid):
    ip = requests.get('https://api.ipify.org/').text  # Change to https://api6.ipify.org/ if you need IPv6
    data = '{"type":"A","name":"' + domain + '","content":"' + ip + '"}'
    url = 'https://api.cloudflare.com/client/v4/zones/' + zoneid + '/dns_records/' + subdomainid
    result = requests.put(url, headers=headers, data=data)
    global response  # makes response global so that it can be used in write_file() and append_file()
    response = result.text


def write_file(domain):  # Use this to create / reset the file output file.
    time_unformatted = time.time()
    timestamp = datetime.fromtimestamp(time_unformatted).strftime('%Y-%m-%d %H:%M:%S')  # Adds timestamp
    f = open('cf_ddns_output.txt', 'w')
    f.write(timestamp + '\t' + domain + ': ' + response)
    f.close()


def append_file(domain):  # This adds to a file after write_file() has been executed.
    time_unformatted = time.time()
    timestamp = datetime.fromtimestamp(time_unformatted).strftime('%Y-%m-%d %H:%M:%S')  # Adds timestamp
    f = open('cf_ddns_output.txt', 'a')
    f.write('\n\n' + timestamp + '\t' + domain + ': ' + response)
    f.close()


while True:  # Makes it so the code never ends. # After the first domain make sure to use append_file!
    # Update the following comment with the credentials from Cloudflare
    '''dynamic_dns('subdomain.example.com', 'zone id', 'subdomain id')
    write_file('subdomain.example.com')
    dynamic_dns('subdomain.example.com', 'zone id', 'subdomain id')
    append_file('subdomain.example.com')'''
    time.sleep(3600)  # Waits 3600 seconds ( executes code again )
    open('cf_ddns_output.txt', 'w').close()
